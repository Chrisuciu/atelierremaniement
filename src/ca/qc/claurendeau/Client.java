package ca.qc.claurendeau;
import java.util.Enumeration;
import java.util.Vector;

/**
 * La classe Client permet d’identifier les clients 
 * du magasin
 * @author 03738
 * 
 */
public class Client {
	private String _nom;
	private Vector _locations = new Vector();

	public Client(String nom) {
		_nom = nom;
	}

	public void ajoutLocation(Location arg) {
		_locations.addElement(arg);
	}

	public String getNom() {
		return _nom;
	}
    
	public String rapport() {
		double mtTotal = 0;
		int pointsBonis = 0;
		Enumeration locations = _locations.elements();
		String result = "Etat des locations effectuées de " + getNom() + " :\n";
        while (locations.hasMoreElements()) {
			double ceMontant = 0;
			Location chaque = (Location) locations.nextElement();
			// calcul montant pour chaque ligne location
            switch (chaque.getOutil().getCodePrix()) {
            case Outil.REGULIER:
                ceMontant += 2;
                if (chaque.getNbJoursLoues() > 2)
                    ceMontant += (chaque.getNbJoursLoues() - 2) * 1.5;
                break;
            case Outil.NOUVEAUTEES:
                ceMontant += chaque.getNbJoursLoues() * 3;
                break;
            case Outil.GROS_OUTILLAGE:
                ceMontant += 1.5;
                if (chaque.getNbJoursLoues() > 3)
                    ceMontant += (chaque.getNbJoursLoues() - 3) * 1.5;
                break;
            }
            // add frequent renter points
			pointsBonis++;
            // ajout de points de boni pour une location de 2 jours
			if ((chaque.getOutil().getCodePrix() == Outil.NOUVEAUTEES)
					&& chaque.getNbJoursLoues() > 1)
				pointsBonis++;
            // détails de la location
			result += "\t" + chaque.getOutil().getTitre() + "\t"
					+ String.valueOf(ceMontant) + "\n";
			mtTotal += ceMontant;
		}
        // Lignes bas de page
		result += "Montant du égale à : " + String.valueOf(mtTotal) + "\n";
		result += "Vous gagnez : " + String.valueOf(pointsBonis)
				+ " points de bonis";
		return result;
	}
}
